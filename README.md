# cv npl

Có thể chạy trên Ubuntu và Centos:
Đã kiểm tra trên: ubuntu 20.04 và Centos 7

- Bước 1: `git clone https://gitlab.com/phulam_2009/cvnpl.git`
- Bước 2: Chỉnh sửa file host chọn remote host và thông tin phù hợp
- Bước 3: `ansible-playbook loadbalance-docker.yml -i hosts`
- Bước 4: Truy cập IP xuất hiện sau khi hoàn thành việc chạy playbook
- Bước 5: Nhấn F5 để chuyển đổi xem CV tiếng Việt và CV Tiếng Anh

Link video demo: https://youtu.be/iOp7l7soUC0

Runs on Ubuntu and Centos:
Tested on: ubuntu 20.04 and Centos 7

- Step 1: `git clone https://gitlab.com/phulam_2009/cvnpl.git`
- Step 2: Edit the host file, select the remote host and the appropriate information
- Step 3: `ansible-playbook loadbalance-docker.yml -i hosts`
- Step 4: Access the IP that appears after finishing running the playbook
- Step 5: Press F5 to switch view Vietnamese and English CV.

```
.
├── ansible.cfg
├── defaults
│   └── main.yml
├── exap.yml
├── hosts
├── loadbalance-docker.yml
├── README.md
├── remove.yml
└── roles
    ├── ansible-role-docker
    │   ├── defaults
    │   │   └── main.yml
    │   ├── handlers
    │   │   └── main.yml
    │   └── tasks
    │       ├── docker-compose.yml
    │       ├── docker-users.yml
    │       ├── main.yml
    │       ├── setup-Debian.yml
    │       └── setup-RedHat.yml
    └── loadbalance-docker
        ├── files
        │   ├── change.sh
        │   ├── Phu-Lam-Nguyen-CV.jpg
        │   └── Phu-Lam-Nguyen-CV-VN_html_cmt.jpg
        ├── tasks
        │   └── main.yml
        └── templates
            ├── change.j2
            ├── default.conf
            ├── docker-compose.j2
            ├── index1.html
            ├── index2.html
            ├── Phu-Lam-Nguyen-CV-EN.html
            └── Phu-Lam-Nguyen-CV-VN.html

10 directories, 25 files


```


