#!/bin/bash
#author: nguyenphulam44@gmail.com
dir="/home/*/compose-nginx"
ipnginx1=`docker ps | grep nginx1 | awk '{print $1}' | xargs docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}'`
ipnginx2=`docker ps | grep nginx2 | awk '{print $1}' | xargs docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}'`
#echo $dir
#echo $ipnginx1
#echo $ipnginx2
sed -i -e "s/IP/$ipnginx1/g" $dir/html1/index.html
sed -i -e "s/IP/$ipnginx2/g" $dir/html2/index.html
cat $dir/html1/index.html
cat $dir/html2/index.html